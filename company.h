#ifndef COMPANY_H
#define COMPANY_H

#include <list>
#include <string>

class Department;
class Employee;

class Company {
    std::list<Department*> departments;
public:
    void addDepartment(Department * department);
    std::list<Department *> getDepartments() const;
    int getTotalSalaries() const;
    Department * getDepartment(const std::string & departmentName) const;
    Employee * getEmployee(const std::string & employeeName) const;
    ~Company();
};

#endif // COMPANY_H
