#include "department.h"
#include "employee.h"

#include <algorithm>

std::list<Employee*> Department::getEmployees() const
{
    return employees;
}
Department::Department(const std::string name)
    : name(name)
{
}
void Department::addEmployee(Employee * employee)
{
    employees.push_back(employee);
}
int Department::getSalary() const
{
    int salary = 0;
    std::for_each(employees.begin(), employees.end(), [&salary](Employee * employee) {
        salary += employee->getSalary();
    });
    return salary;
}

std::string Department::getName() const
{
    return name;
}

Employee * Department::getEmployee(const std::string & employeeName) const
{
    auto employee = std::find_if(employees.cbegin(),
                                 employees.cend(),
                                 [&employeeName](const Employee * employee) {
        return employee && (employee->getName() == employeeName);
    });
    return employee == employees.cend() ? nullptr : *employee;
}

Department::~Department()
{
    std::for_each(employees.begin(), employees.end(), [](Employee * employee) {
        delete employee;
    });
}
