#include "company.h"
#include "department.h"
#include "employee.h"

#include <algorithm>

void Company::addDepartment(Department * department)
{
    departments.push_back(department);
}
std::list<Department *> Company::getDepartments() const
{
    return departments;
}
int Company::getTotalSalaries() const
{
    int salary = 0;
    std::for_each(departments.begin(), departments.end(), [&salary](Department * department) {
        salary += department->getSalary();
    });
    return salary;
}
Department * Company::getDepartment(const std::string & departmentName) const
{
    auto department = std::find_if(departments.cbegin(),
                                   departments.cend(),
                                   [&departmentName](const Department * department) {
            return department && (department->getName() == departmentName);
    });
    return department == departments.cend() ? nullptr : *department;
}

Employee * Company::getEmployee(const std::string & employeeName) const
{
    for(auto departmentIt = departments.cbegin(); departmentIt != departments.cend(); ++departmentIt)
    {
        Department * department = *departmentIt;
        Employee * employee = department ? department->getEmployee(employeeName) : nullptr;
        if (employee)
        {
            return employee;
        }
    }
    return nullptr;
}
Company::~Company()
{
    std::for_each(departments.begin(), departments.end(), [](Department * department) {
        delete department;
    });
}
