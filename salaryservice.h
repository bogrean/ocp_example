#ifndef SALARYSERVICE_H
#define SALARYSERVICE_H

#include <string>

class SalaryService {
public:
    virtual ~SalaryService() {}
    virtual int getSalaryForEmployee(const std::string & employeeName) = 0;
    virtual int getSalaryForDepartment(const std::string & departmentName) = 0;
    virtual int getSalaryForCompany() = 0;
};

#endif // SALARYSERVICE_H
