#ifndef SALARYSERVICEIMPLEMENTATION_H
#define SALARYSERVICEIMPLEMENTATION_H

#include "salaryservice.h"
#include <string>
class Company;

class SalaryServiceImplementation : public SalaryService
{
    Company * company = nullptr;
public:
    SalaryServiceImplementation(Company * company);
    int getSalaryForEmployee(const std::string & employeeName) override;
    int getSalaryForDepartment(const std::string & departmentName) override;
    int getSalaryForCompany() override;
};

#endif // SALARYSERVICEIMPLEMENTATION_H
