#include "salaryserviceimplementation.h"
#include "company.h"
#include "employee.h"
#include "department.h"
#include <algorithm>

SalaryServiceImplementation::SalaryServiceImplementation(Company * company)
    : company(company)
{
}
int SalaryServiceImplementation::getSalaryForEmployee(const std::string & employeeName)
{
    Employee * employee = company->getEmployee(employeeName);
    return employee ? employee->getSalary() : 0;
}
int SalaryServiceImplementation::getSalaryForDepartment(const std::string & departmentName)
{
    Department * department = company->getDepartment(departmentName);
    return department ? department->getSalary() : 0;
}
int SalaryServiceImplementation::getSalaryForCompany()
{
    return company->getTotalSalaries();
}
