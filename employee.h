#ifndef EMPLOYEE_H
#define EMPLOYEE_H

#include <string>


class Employee
{
    std::string name;
    int salary;
public:
    Employee(const std::string name, int salary);
    std::string getName() const;
    int getSalary() const;
};

#endif // EMPLOYEE_H
