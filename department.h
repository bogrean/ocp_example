#ifndef DEPARTMENT_H
#define DEPARTMENT_H

#include <string>
#include <list>

class Employee;

class Department
{    
    std::string name;
    std::list<Employee*> employees;
public:
    std::list<Employee*> getEmployees() const;
    Department(const std::string name);
    void addEmployee(Employee * employee);
    int getSalary() const;
    std::string getName() const;
    Employee * getEmployee(const std::string & employeeName) const;
    ~Department();
};

#endif // DEPARTMENT_H
