#include <iostream>
#include <string>
#include <list>
#include <algorithm>

#include "employee.h"
#include "department.h"
#include "company.h"
#include "salaryservice.h"
#include "salaryserviceimplementation.h"
// We need a program to keep track of the salary of all 
// employees in the company: see each individual, 
// each department and totals


int main(int /*argc*/, char **/*argv[]*/)
{ 
    Employee * gicu = new Employee("Gicu", 7854);
    Employee * mishu = new Employee("Mishu", 45887);
    Employee * kati = new Employee("Kati", 7548);
    Department * theGood = new Department("TheGood");
    Department * theBad = new Department("TheBad");
    Department * theUgly = new Department("TheUgly");
    Company * company = new Company();
    theGood->addEmployee(gicu);
    theGood->addEmployee(kati);
    theBad->addEmployee(mishu);
    company->addDepartment(theGood);
    company->addDepartment(theBad);
    company->addDepartment(theUgly);

    SalaryService * service = new SalaryServiceImplementation(company);

    char c = 1;
    while (c != '0')
    {
        std::cout << std::endl << "enter salary option you want to see: " << std::endl
                  << "1. Company totals" << std::endl
                  << "2. Department totals" << std::endl
                  << "3. employee salary" << std::endl
                  << "0. Get me out of here" <<  std:: endl
                  << "\tYour option: ";
        std::cin >> c;
        switch(c)
        {
            case '1':
                std::cout << "Company total: " << service->getSalaryForCompany();
                break;
            case  '2':
            {
                std::string departmenName;
                std::cout << std::endl << "Department name: ";
                std::cin >> departmenName;
                std::cout << "Department total: " << service->getSalaryForDepartment(departmenName);
                break;
            }
            case  '3':
            {
                std::string employeeName;
                std::cout << std::endl << "Employee name: ";
                std::cin >> employeeName;
                std::cout << "Employee salary: " << service->getSalaryForEmployee(employeeName);
                break;
            }
            default:
                break;
        }
    }
    delete company;
    delete service;
    return 0;
}
