#include "employee.h"

Employee::Employee(const std::string name, int salary)
    : name(name),
    salary(salary){
    
}
std::string Employee::getName() const {
    return name;
}
int Employee::getSalary() const {
    return salary;
}
